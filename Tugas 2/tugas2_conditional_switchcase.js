console.log("=================== Tugas Switch Case ===================")

var tanggal = 17; 
var bulan = 8; 
var tahun = 1945;

function formatTanggal(tanggal, bulan, tahun){
    switch(true){
        case(tanggal < 1 || tanggal > 31): {
            console.log('Input tanggal salah')
            break;
        }
        case(tahun < 1900 || tahun > 2200): {
            console.log("Input tahun salah")
            break;
        }
        case(bulan < 1 || bulan > 12): {
            console.log('Input bulan salah')
            break;
        }
        default: {
            switch(bulan){
                case 1 :
                    strBulan = 'Januari'
                    break
                case 2 :
                    strBulan = 'Februari'
                    break
                case 3 :
                    strBulan = 'Maret'
                    break
                case 4 :
                    strBulan = 'April'
                    break
                case 5 :
                    strBulan = 'Mei'
                    break
                case 6 :
                    strBulan = 'Juni'
                    break
                case 7 :
                    strBulan = 'Juli'
                    break
                case 8 :
                    strBulan = 'Agustus'
                    break
                case 9 :
                    strBulan = 'September'
                    break
                case 10 :
                    strBulan = 'Oktober'
                    break
                case 11 :
                    strBulan = 'November'
                    break
                case 12 :
                    strBulan = 'Desember'
                    break
            }
            console.log(tanggal+' '+strBulan+' '+tahun)
        }
        break
    }    
}

formatTanggal(1,1,1900)
formatTanggal(12,6,2020)
formatTanggal(31,12,2200)
formatTanggal(33,1,2020)
formatTanggal(12,13,2020)
formatTanggal(12,1,2220)