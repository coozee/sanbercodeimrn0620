console.log('\n-------------------------- No.1 Looping while ------------------------------\n')

console.log('LOOPING PERTAMA')
var deret = 0;
while(deret  < 20) { 
  deret += 2; 
  console.log(deret + ' - I love coding')
}

console.log('LOOPING KEDUA')
var deret = 20;
while(deret  > 0) { 
  console.log(deret + ' - I will become a mobile developer')
  deret -= 2; 
}

console.log('\n-------------------------- No. 2 Looping menggunakan for ------------------------------\n')

console.log('OUTPUT')
var str = '';
for(var angka = 1; angka <= 20; angka++) {
    switch(true){
        case(angka % 3 == 0 && ((angka / 3 ) % 2 != 0)) :
            str = 'I Love Coding'
            break
        case(angka % 2 == 0) :
            str = 'Berkualitas'
            break            
        default :
            str = 'Santai'
        break
    }
    console.log(angka + ' - ' + str);
} 


console.log('\n-------------------------- No. 3 Membuat Persegi Panjang # ------------------------------\n')

function buatPersegiPanjang(panjang, lebar){
    var baris = ''
    for(var i = 0; i < lebar; i++) {
        baris = ''
        for(var j = 0; j < panjang; j++){
            baris += '#';
        }
        console.log(baris);
    } 
}

buatPersegiPanjang(8,4)

console.log('\n-------------------------- No. 4 Tangga ------------------------------\n')

function buatSegitiga(sisi){
    var baris = ''
    for(var i = 0; i < sisi; i++) {
        baris += '#';
        console.log(baris);
    } 
}

buatSegitiga(7)

console.log('\n-------------------------- No. 5 Membuat Papan Catur ------------------------------\n')

function buatPapanCatur(sisi){
    var baris = ''
    for(var i = 0; i < sisi; i++) {
        baris = ''
        for(var j = 0; j < sisi; j++){
            if((i+j)%2 == 0){
                baris += ' '
            } else {
                baris += '#'
            }
        }
        console.log(baris);
    } 
}

buatPapanCatur(8)