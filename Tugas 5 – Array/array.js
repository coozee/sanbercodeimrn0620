
console.log('\n-------------------------- Soal No. 1 (Range) ------------------------------\n')
function range(startNum, finishNum){
    var output = []

    if(!startNum || !finishNum){
        output= -1
    } else {
        if(startNum > finishNum) {
            for(var i=startNum;i>=finishNum;i--){
                output.push(i)
            }
        } else {
            for(var i=startNum;i<=finishNum;i++ ){
                output.push(i)
            }
        }
    }

    return output
}
 
console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 


console.log('\n-------------------------- Soal No. 2 (Range with Step) ------------------------------\n')
function rangeWithStep(startNum, finishNum, step) {
    var output = []

    if(!startNum || !finishNum){
        output= -1
    } else {
        if(startNum > finishNum) {
            for(var i=startNum;i>=finishNum;i-=step){
                output.push(i)
            }
        } else {
            for(var i=startNum;i<=finishNum;i+=step){
                output.push(i)
            }
        }
    }

    return output
}

console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 


console.log('\n-------------------------- Soal No. 3 (Sum of Range) ------------------------------\n')

function sum(startNum, finishNum, step) {
    if(!step){
        step = 1
    }
    if(!finishNum){
        finishNum=startNum
    }
    var arr = rangeWithStep(startNum, finishNum, step)
    var output = 0
    for(var i=0; i<arr.length; i++){
        output += arr[i]
    }
    return output
}

console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 


console.log('\n-------------------------- Soal No. 4 (Array Multidimensi) ------------------------------\n')
//contoh input
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]

function dataHandling(){
    for(var i=0; i<input.length; i++){
        console.log('Nomor ID:  ' + input[i][0])
        console.log('Nama Lengkap:  ' + input[i][1])
        console.log('TTL:  ' + input[i][2] + ' ' + input[i][3])
        console.log('Hobi:  ' + input[i][4])
        console.log()
    }
}

dataHandling()

console.log('\n-------------------------- Soal No. 5 (Balik Kata) ------------------------------\n')

function balikKata(kata){
    kata_terbalik = ''
    for(var i=kata.length-1; i>=0; i--){
        kata_terbalik += kata[i]
    }
    return kata_terbalik
}

console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I

console.log('\n-------------------------- Soal No. 6 (Metode Array) ------------------------------\n')

function dataHandling2(arr){
    arr.splice(1,1, arr[1]+' Elsharawy')
    arr.splice(2,1, 'Provinsi '+arr[2])
    arr.splice(4,1,'Pria')
    arr.splice(5,0,'SMA Internasional Metro')
    console.log(arr)

    var arrTanggal = arr[3].split('/')
    var noBulan = arrTanggal[1]
    var namaBulan
    switch (noBulan) {
        case '01':
            namaBulan = 'Januari'
            break;
        case '02':
            namaBulan = 'Februari'
            break;
        case '03':
            namaBulan = 'Maret'
            break;
        case '04':
            namaBulan = 'April'
            break;
        case '05':
            namaBulan = 'Mei'
            break;
        case '06':
            namaBulan = 'Juni'
            break;
        case '07':
            namaBulan = 'Juli'
            break;
        case '08':
            namaBulan = 'Agustus'
            break;
        case '09':
            namaBulan = 'September'
            break;
        case '10':
            namaBulan = 'Oktober'
            break;
        case '11':
            namaBulan = 'November'
            break;
        case '12':
            namaBulan = 'Desember'
            break;    
        default:
            break;
    }
    console.log(namaBulan)
 
    arrTanggal_sort = arrTanggal.slice()
    arrTanggal_sort.sort(function (value1, value2) { return value2 - value1 } );
    console.log(arrTanggal_sort)

    console.log(arrTanggal.join('-'))

    nama = arr[1].split('')
    nama = nama.slice(0,14)
    nama = nama.join('')
    console.log(nama)
}

arr = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"]  
dataHandling2(arr)

// suci