
console.log('\n-------------------------- Soal No. 1 (Array to Object) ------------------------------\n')
var now = new Date()
var thisYear = now.getFullYear() // 2020 (tahun sekarang)

function arrayToObject(arr) {
    // Code di sini 
    var personObj = {}
    for(var i = 0 ; i < arr.length; i++){
        personObj.firstName = arr[i][0]
        personObj.lastName = arr[i][1]
        personObj.gender = arr[i][2]

        if(!arr[i][3] || arr[i][3] > thisYear){
            personObj.age = "Invalid birth year"
        } else {
            personObj["age"] = thisYear - arr[i][3]
        }

        console.log((i+1) + '. ' + personObj.firstName + ' ' + personObj.lastName + ':')
        console.log(personObj)
    }
    console.log()
}
 
// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
// Error case 
arrayToObject([]) // ""

console.log('\n-------------------------- Soal No. 2 (Shopping Time) ------------------------------\n')
function shoppingTime(memberId, money) {
    // you can only write your code here!
    if(!memberId){
        return "Mohon maaf, toko X hanya berlaku untuk member saja"
    } else if(money < 50000){
        return "Mohon maaf, uang tidak cukup"
    }

    listSale = [["Sepatu brand Stacattu", 1500000],
                    ["Baju brand Zoro", 500000],
                    ["Baju brand H&N", 250000],
                    ["Sweater brand Uniklooh", 175000],
                    ["Casing Handphone", 50000]]
    changeMoney = money
    listPurchased = []

    for(var i = 0; i < listSale.length; i++){
        if(changeMoney >= listSale[i][1]){
            listPurchased.push(listSale[i][0])
            changeMoney = changeMoney - listSale[i][1]
        }
    }

    var objPurchase = {    
        "memberId" : memberId,
        "money" : money,
        "listPurchased" : listPurchased,
        "changeMoney" : changeMoney
    }

    return objPurchase
  }
   
  // TEST CASES
  console.log(shoppingTime('1820RzKrnWn08', 2475000));
    //{ memberId: '1820RzKrnWn08',
    // money: 2475000,
    // listPurchased:
    //  [ 'Sepatu Stacattu',
    //    'Baju Zoro',
    //    'Baju H&N',
    //    'Sweater Uniklooh',
    //    'Casing Handphone' ],
    // changeMoney: 0 }
  console.log(shoppingTime('82Ku8Ma742', 170000));
  //{ memberId: '82Ku8Ma742',
  // money: 170000,
  // listPurchased:
  //  [ 'Casing Handphone' ],
  // changeMoney: 120000 }
  console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
  console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
  console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

console.log('\n-------------------------- Soal No. 3 (Naik Angkot) ------------------------------\n')

function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    //your code here
    var orang = []
    for(var i = 0 ; i < arrPenumpang.length; i++){
        orang[i] = {}
        orang[i].penumpang = arrPenumpang[i][0]
        orang[i].naikDari = arrPenumpang[i][1]
        orang[i].tujuan = arrPenumpang[i][2]
        orang[i].bayar = 2000 * (rute.indexOf(arrPenumpang[i][2]) - rute.indexOf(arrPenumpang[i][1]))
    }
    return orang
  }
   
  //TEST CASE
  console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
  // [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
  //   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
   
  console.log(naikAngkot([])); //[] 