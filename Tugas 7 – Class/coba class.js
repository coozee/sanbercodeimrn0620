
// Tidak diberi nama 
var Car = class {
    constructor(brand, factory) {
        this.brand = brand
        this.factory = factory
    }
}
 
console.log(Car.name) // Car
 
// Diberi nama
var Car = class Car2 {
    constructor(brand, factory) {
        this.brand = brand
        this.factory = factory
    }
}
console.log(Car.name) // Car2

class Car3 {
    constructor(brand) {
      this.carname = brand;
    }
    present() {
      return "I have a " + this.carname;
    }
  }
  
  mycar = new Car3("Ford");
  console.log(mycar.present()) // I have a Ford