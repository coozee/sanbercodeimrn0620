var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]

// Lanjutkan code untuk menjalankan function readBooksPromise 
var promise = readBooksPromise (10000, books[0])

promise.then(function (fulfilled) {
    readBooksPromise (fulfilled, books[1]).then(function (fulfilled) {
        readBooksPromise (fulfilled, books[2]).then(function (fulfilled) {
            console.log('Selesai')
        }).catch(function (e) {
            console.log(e);
        });
    }).catch(function (e) {
        console.log(e);
    });
}).catch(function (e) {
    console.log(e);
});