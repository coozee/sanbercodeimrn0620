console.log('------------------- Let + Const') 

let x = 1;
 
if (x === 1) {
  let x = 2;
 
  console.log(x);
  // expected output: 2
}
 
console.log(x); // 1 

const number = 42;
//number = 100; // Uncaught TypeError: Assignment to constant variable. 

console.log('------------------- Default Parameters') 
function multiply(a, b = 1) {
    return a * b;
  }
   
  console.log(multiply(5, 2));
  // expected output: 10
   
  console.log(multiply(5));
  // expected output: 5 

console.log('------------------- Destructuring') 
let studentName = {
    firstName: 'Peter',
    lastName: 'Parker'
};
 
const { firstName, lastName } = studentName;
 
console.log(firstName); // Peter
console.log(lastName); // Parker 
console.log('------------------- Rest Parameters + Spread Operator') 
// Rest Parameters
 
let scores = ['98', '95', '93', '90', '87', '85']
let [first, second, third, ...restOfScores] = scores;
 
console.log(first) // 98
console.log(second) // 95
console.log(third) // 93
console.log(restOfScores) // [90, 87, 85] 


console.log('------------------- ') 
let array1 = ['one', 'two']
let array2 = ['three', 'four']
let array3 = ['five', 'six']
 
// ES5 Way / Normal Javascript
 
//var combinedArray = array1.concat(array2).concat(array3)
//console.log(combinedArray) // ['one', 'two', 'three', 'four', 'five', 'six']
 
// ES6 Way 
 
let combinedArray = [...array1, ...array2, ...array3]
console.log(combinedArray) // ['one', 'two', 'three', 'four', 'five', 'six'] 

console.log('------------------- Enhanced object literals') 
const fullName = 'Zell Liew'
 
//const Zell = {
//  fullName: fullName
//}
 
// ES6 way
const Zell = {
  fullName
}

console.log('------------------- Template Literals')
const namaPertama = 'Zell'
const namaKeluarga = 'Liew'
const teamName = 'unaffiliated'
 
const theString = `${namaPertama} ${namaKeluarga}, ${teamName}`
 
console.log(theString) // Zell Liew, unaffiliated