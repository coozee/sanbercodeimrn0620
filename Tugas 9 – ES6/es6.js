console.log('\n---------------------- 1. Mengubah fungsi menjadi fungsi arrow -------------------------\n')

golden = () => {
    console.log("this is golden!!")
}

golden()

console.log('\n---------------------- 2. Sederhanakan menjadi Object literal di ES6 -------------------------\n')

const newFunction = ((firstName, lastName) => {
    return {
      firstName,
      lastName,
      fullName(){
        console.log(`${firstName} ${lastName}`)
        return 
      }
    }
})
   
//Driver Code 
newFunction("William", "Imoh").fullName()

console.log('\n---------------------- 3. Destructuring -------------------------\n')
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}

const { firstName, lastName, destination, occupation, spell } = newObject;

// Driver code
console.log(firstName, lastName, destination, occupation)

console.log('\n---------------------- 4. Array Spreading -------------------------\n')

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
//const combined = west.concat(east)
let combined = [...west, ...east]
//Driver Code
console.log(combined)

console.log('\n---------------------- 5. Template Literals -------------------------\n')

const planet = "earth"
const view = "glass"
/*var before = 'Lorem ' + view + 'dolor sit amet, ' +  
    'consectetur adipiscing elit,' + planet + 'do eiusmod tempor ' +
    'incididunt ut labore et dolore magna aliqua. Ut enim' +
    ' ad minim veniam'
    */

const before = `Lorem ${view}dolor sit amet, consectetur adipiscing elit,${planet}do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`

// Driver Code
console.log(before) 